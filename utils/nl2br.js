export default function nl2br( text ){
    return text.replace(/\r?\n/g, '<br />');
}
