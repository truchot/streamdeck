const _ = require('lodash')
const plugin = require('tailwindcss/plugin');

module.exports = plugin(function ({addUtilities, theme}) {
    const hasBackgroundColorUtilities = _.map(theme('colors'),(value, key) => {
        return {
            [`.has-${key}-background-color`]: {
                backgroundColor: `${value}`
            }
        }
    })

    const hasColorUtilities = _.map(theme('colors'), (value, key) => {
        return {
            [`.has-${key}-color`]: {
                color: `${value}`
            }
        }
    })

    addUtilities(hasBackgroundColorUtilities)
    addUtilities(hasColorUtilities)
})
